# Redis-chat

Redis-chat на java
### Использование
В начале надо запустить docker контейнер Redis с портом 9000 (можно изменить в программе подключаемый порт)
<Br>
<br>
Для запуска программы:
<br>
**IDE**: запустить RedisChat.java
<br>
**Maven**: `mvn clean package`. Затем `java -jar redis-sbt.jar`
<br>
Параллельно открыть redis-cli для проверки чата (`redis-cli subscribe` <chat> и `redis-cli publish <chat> <message>`)
<br>
**Docker**: Запустить контейнер с Redis: `docker run --name <redis-name> redis`
<br>
В текущей директории ввести команду `docker build <image-name> .`
<br>

Запустить несколько контейнеров из новосозданного образа: 
```docker run -it --name <container-name> --link <redis-name>:redis-server <image-name> /home/start.sh```
