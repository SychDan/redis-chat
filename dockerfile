FROM maven
COPY . /home/
RUN cd /home/ && mvn clean package
RUN chmod 777 -R /home/start.sh
RUN chmod 777 -R /home/target/redis-sbt-1.0.jar
