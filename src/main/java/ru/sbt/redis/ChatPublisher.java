package ru.sbt.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Publisher of Redis message
 */
public class ChatPublisher {

    private static final Logger logger = LoggerFactory.getLogger(ChatPublisher.class);

    private final Jedis publisherJedis;

    private final String channel;

    private final String username;

    public ChatPublisher(Jedis jedis, String channel, String username) {
        this.publisherJedis = jedis;
        this.channel = channel;
        this.username = username;
    }

    public void start() {
        logger.info("Type your message (\"quit\" for terminate)");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            while (true) {
                String line = reader.readLine();

                if (!"quit".equals(line)) {
                    publisherJedis.publish(channel, username + ": " + line);
                } else {
                    break;
                }
            }

        } catch (IOException e) {
            logger.error("IO failure while reading input, e");
        }
    }
}