package ru.sbt.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.JedisPubSub;

/**
 * Subscriber of Redis message
 */
public class ChatSubscriber extends JedisPubSub {

    private static Logger logger = LoggerFactory.getLogger(ChatSubscriber.class);

    private String room;

    public ChatSubscriber(String room) {
        this.room = room;
    }

    @Override
    public void onMessage(String channel, String message) {
        logger.info(message);
    }

    @Override
    public void onPMessage(String pattern, String channel, String message) {
        logger.info("Message onPMessage");
    }

    @Override
    public void onSubscribe(String channel, int subscribedChannels) {
        logger.info("Welcome to " + room + " (^_^)/");
    }

    @Override
    public void onUnsubscribe(String channel, int subscribedChannels) {
        logger.info("Goodbye");
    }

    @Override
    public void onPUnsubscribe(String pattern, int subscribedChannels) {
        logger.info("Message onPUnsubscribe");
    }

    @Override
    public void onPSubscribe(String pattern, int subscribedChannels) {
        logger.info("Message onPSubscribe");
    }
}