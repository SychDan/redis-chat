package ru.sbt.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.Scanner;

/**
 * Simple chat
 * For usage, use redis-cli, this program or jar-file of this program
 */
public class RedisChat {

    private static final Logger logger = LoggerFactory.getLogger(RedisChat.class);

    public static final String REDIS_HOST = "redis-server";

    public static final int REDIS_PORT = 6379;

    private static ChatSubscriber chatSubscriber;

    public static void main(String[] args) {
        JedisPool jedisPool = new JedisPool(REDIS_HOST, REDIS_PORT);
        System.out.print("Welcome." + System.lineSeparator() + "Enter your name: ");
        Scanner scanner = new Scanner(System.in);

        String username = scanner.next();
        String chatRoom = enteringInRoom(scanner, jedisPool.getResource());

        ChatPublisher chatPublisher = new ChatPublisher(jedisPool.getResource(), chatRoom, username);
        chatPublisher.start();

        shutdown();
    }

    /**
     * Entering in room
     * @return room name.
     */
    private static String enteringInRoom(Scanner scanner, Jedis jedis) {
        while (true) {
            System.out.print("Enter chat room: ");
            String room = scanner.next();
            List<String> existsChannels = jedis.pubsubChannels("*");

            if (existsChannels.contains(room)) {
                System.out.println("Room \"" + room + "\" is exists");
            } else {
                System.out.println("Room \"" + room + "\" not exists. Do you want create a new room? Y|N");
                String answer = scanner.next();
                if (!answer.toLowerCase().equals("y")) {
                    continue;
                }
            }

            chatSubscriber = new ChatSubscriber(room);
            subscribe(jedis, chatSubscriber, room);
            return room;
        }
    }

    /**
     * Subscribe to channel (room) via Redis
     */
    private static void subscribe(Jedis subscriberJedis, ChatSubscriber chatSubscriber, String room) {
        Thread thread = new Thread(() -> {
            try {
                logger.info("Entering to  chat \"" + room + "\"...");
                subscriberJedis.subscribe(chatSubscriber, room);
                logger.info("End  chat.");
            } catch (Exception e) {
                logger.error("Subscribing failed.", e);
            }
        });
        thread.start();
    }

    /**
     * End of chat
     */
    private static void shutdown() {
        chatSubscriber.unsubscribe();
    }
}
